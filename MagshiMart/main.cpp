#include"Customer.h"
#include<map>
#include <string>

enum menuOptions { NEW_CUSTOMER = 1, UPDATE_CUSTOMER, PRINT_MOST, EXIT };
enum updateOptions { ADD_ITEMS = 1, REMOVE_ITEMS, EXIT1 };

void printMenu();
void printItems();
void printUpdateOptions();
void newCustomer(map<string, Customer>& abcCustomers, Item* itemList);
void modifyItems(Customer& temp, Item* itemList, bool op);
void updateCustomer(map<string, Customer> abcCustomers, Item* itemList);
void paysMost(map<string, Customer> abcCustomers);

int main()
{
	bool stop = false;
	int option = 0;
	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	while (!stop)
	{
		//do while until entred correct option
		do {
			system("cls");
			printMenu();
			cin >> option;
		} while (option < NEW_CUSTOMER || option > EXIT);

		switch (option)
		{
		case NEW_CUSTOMER: //adds new customer
			newCustomer(abcCustomers, itemList);
			break;
		case UPDATE_CUSTOMER: //updates existing customer
			updateCustomer(abcCustomers, itemList);
			break;
		case PRINT_MOST: //prints the customer that pays the most
			paysMost(abcCustomers);
			break;
		case EXIT:
			stop = true;
			break;
		}
	}

	system("pause");

	return 0;
}

/*
Prints Main Menu
*/
void printMenu()
{
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1. to sign as customer and but items" << endl;
	cout << "2. to update existing customer's items" << endl;
	cout << "3. to print the customer who pays the most" << endl;
	cout << "4. exit" << endl;
}

/*
Prints Item list
*/
void printItems()
{
	cout << "The items you can buy/remove are: (0 to exit)" << endl;
	cout << "1. Milk price: 5.3" << endl;
	cout << "2. Cookies price: 12.6" << endl;
	cout << "3. Bread price: 8.9" << endl;
	cout << "4. Chooclate price: 7.0" << endl;
	cout << "5. Cheese price: 15.3" << endl;
	cout << "6. Rice price: 6.2" << endl;
	cout << "7. Fish price: 31.65" << endl;
	cout << "8. Chicken price: 25.99" << endl;
	cout << "9. Cucmber price: 1.21" << endl;
	cout << "10. Tomato price: 2.32" << endl;
	cout << "What item you like to buy? Input:" << endl;
}

/*
Creates new customer and adds items to heis item list
*/
void newCustomer(map<string, Customer>& abcCustomers, Item* itemList)
{
	string name;
	cout << "Enter your name: ";
	cin >> name;

	if (!abcCustomers.count(name))
	{
		Customer temp(name);
		modifyItems(temp, itemList, true);
		abcCustomers.insert(pair<string, Customer>(name, temp));
	}
	else
	{
		cout << "The Customer already Exists!" << endl;
		system("pause");
	}
	
}

/*
Modifies customers items
op = true - add items
op = false - remove items
*/
void modifyItems(Customer& customer, Item* itemList, bool op)
{
	int option = 1;
	while (option != 0)
	{
		do
		{
			system("cls");
			printItems();
			cin >> option;
		} while (option < 0 || option > 10);
		if (option != 0) //if option is valid
		{
			if (op) //op is true add items
			{
				customer.addItem(itemList[option - 1]);
			}
			else //op is false remove items
			{
				customer.removeItem(itemList[option - 1]);
			}
		}
	}
}

/*
Updates Customer's items
*/
void updateCustomer(map<string, Customer> abcCustomers, Item* itemList)
{
	int option;
	bool stop = false;
	string name;
	map<string, Customer>::iterator it;

	cout << "Enter customer's name: ";
	cin >> name;

	if (abcCustomers.count(name)) //checks if the customer exists
	{
		while (!stop)
		{
			do {
				system("cls");
				printUpdateOptions();
				cin >> option;
			} while (option < 1 || option > 3);

			it = abcCustomers.find(name);
			Customer temp = it->second;
			switch (option)
			{
			case ADD_ITEMS: //adds items to the customer
				modifyItems(temp, itemList, true);
				break;
			case REMOVE_ITEMS: //removes items from the customer
				modifyItems(temp, itemList, false);
				break;
			case EXIT1:
				stop = true;
				break;
			default:
				break;
			}
			abcCustomers.erase(name); //erases to reinsert
			abcCustomers.insert(pair<string, Customer>(name, temp));
		}
	}
	else
	{
		cout << "Customer Does not Exists!" << endl;
		system("pause");
	}
}

/*
Prints update customer options
*/
void printUpdateOptions()
{
	cout << "1. Add items" << endl;
	cout << "2. Remove items" << endl;
	cout << "3. Back to menu" << endl;
}

void paysMost(map<string, Customer> abcCustomers)
{
	if (!abcCustomers.empty()) //checks if any customers exists
	{
		Customer most = abcCustomers.begin()->second;
		for (auto it : abcCustomers)
		{
			if (it.second.totalSum() > most.totalSum())
			{
				most = it.second;
			}
		}
		cout << "The Customer that pays the most is: " << most.getName() << " with a total sum of: " << most.totalSum() << endl;
		system("pause");
	}
	else
	{
		cout << "No customers exist please add a new customer!!" << endl;
		system("pause");
	}
}