#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice) : _name(name), _serialNumber(serialNumber), _unitPrice(unitPrice), _count(1)
{
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item & other) const
{
	if (serialStr_to_serialInt() < other.serialStr_to_serialInt())
	{
		return true;
	}
	return false;
}

bool Item::operator>(const Item & other) const
{
	if (serialStr_to_serialInt() > other.serialStr_to_serialInt())
	{
		return true;
	}
	return false;
}

int Item::serialStr_to_serialInt() const
{
	int serial = 0;
	for (int i = 0; i < _serialNumber.length(); i++)
	{
		serial = serial * 10 + (serial + (_serialNumber[i] - '0'));
	}
	return serial;
}

bool Item::operator==(const Item& other) const
{
	if (_serialNumber == other._serialNumber)
	{
		return true;
	}
	return false;
}

void Item::setName(const string name)
{
	_name = name;
}

void Item::setSerialNumber(const string serialNumber)
{
	_serialNumber = serialNumber;
}

void Item::setUnitPrice(const double unitPrice)
{
	_unitPrice = unitPrice;
}

void Item::setCount(const int count)
{
	_count = count;
}

string Item::getName() const
{
	return _name;
}

string Item::getSerialNumber() const
{
	return _serialNumber;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}

int Item::getCount() const
{
	return _count;
}
