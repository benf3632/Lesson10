#include "Customer.h"

Customer::Customer(string name) : _name(name)
{
}

Customer::Customer()
{
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	for (auto it : _items)
	{
		sum += it.totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	set<Item>::iterator it;
	if ((it = _items.find(item)) != _items.end())
	{
		Item i = *it;
		i.setCount(i.getCount() + 1);
		_items.erase(it);
		_items.insert(i);
	}
	else
	{
		_items.insert(item);
	}
}

void Customer::removeItem(Item item)
{
	set<Item>::iterator it;
	if ((it = _items.find(item)) != _items.end())
	{
		if (it->getCount() > 1)
		{
			Item i = *it;
			i.setCount(i.getCount() - 1);
			_items.erase(it);
			_items.insert(i);
		}
		else
		{
			_items.erase(item);
		}
	}
	
}

string Customer::getName() const
{
	return _name;
}

set<Item> Customer::getSet() const
{
	return _items;
}

void Customer::setName(const string name)
{
	_name = name;
}